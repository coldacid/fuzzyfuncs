﻿// FuzzyFuncs: A C# library for generic fuzzy logic systems
// Copyright (c) 2013 Chris Charabaruk <chrisNOSPAM@charabaruk.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Charabaruk.FuzzyFuncs
{
    public class FuzzySystem<TOutput>
    {
        Dictionary<Func<double, double>, TOutput> _functionMap;
        Random _fuzzer;

        /// <summary>
        /// Create a new system for fuzzily determining values.
        /// </summary>
        public FuzzySystem()
        {
            _functionMap = new Dictionary<Func<double, double>, TOutput>();
            _fuzzer = new Random();
        }

        /// <summary>
        /// Create a new system for fuzzily determining values.
        /// </summary>
        /// <param name="functionMap">An initial collection of outputs mapped to functions for determining probabilities.</param>
        public FuzzySystem(Dictionary<Func<double, double>, TOutput> functionMap)
            : this()
        {
            foreach (var pair in functionMap)
                _functionMap.Add(pair.Key, pair.Value);
        }

        public FuzzySystem(int seed)
            : this()
        {
            _fuzzer = new Random(seed);
        }

        public FuzzySystem(Dictionary<Func<double, double>, TOutput> functionMap, int seed)
            : this(functionMap)
        {
            _fuzzer = new Random(seed);
        }

        /// <summary>
        /// Register a potential output with a given probability function.
        /// </summary>
        /// <param name="function">A function object that takes an input value and returns a possibility for the given potential output within the range (0, 1).</param>
        /// <param name="value">The potential output mapped to the given probability function.</param>
        public void Register(Func<double, double> function, TOutput value)
        {
            _functionMap.Add(function, value);
        }

        /// <summary>
        /// Remove a given probability function.
        /// </summary>
        /// <param name="function">The probability function to remove.</param>
        public void Remove(Func<double, double> function)
        {
            if (_functionMap.ContainsKey(function))
                _functionMap.Remove(function);
        }

        /// <summary>
        /// Remove all probability functions for a given output.
        /// </summary>
        /// <param name="value">The output value to eliminate from the fuzzy system.</param>
        public void Remove(TOutput value)
        {
            var keys = new List<Func<double, double>>();

            foreach (var pair in _functionMap)
            {
                if (pair.Value.Equals(value))
                    keys.Add(pair.Key);
            }

            foreach (var key in keys)
                _functionMap.Remove(key);
        }

        /// <summary>
        /// Get an output based on the given input.
        /// </summary>
        /// <param name="input">The input value to use to determine which potential outputs to return.</param>
        /// <returns>A previously mapped output value for which the probability is non-zero for the given output.</returns>
        /// <remarks>
        /// Outputs may not be the same for a given input with multiple calls of this method, depending on the probability functions used in a system.
        /// </remarks>
        /// <exception cref="T:System.ArgumentOutOfRangeException">Thrown when no outputs are possible for the provided input.</exception>
        public TOutput GetResult(double input)
        {
            TOutput output;

            if (TryGetResult(input, out output))
            {
                return output;
            }
            else
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Get an output based on the given input.
        /// </summary>
        /// <param name="input">The input value to use to determine which potential outputs to return.</param>
        /// <param name="output">A previously mapped output value for which the probability is non-zero for the given output, or default(TOutput) if no outputs are possible for the provided input.</param>
        /// <returns>Returns true if the value of output is valid (the provided input allows for a potential output); false otherwise.</returns>
        public bool TryGetResult(double input, out TOutput output)
        {
            var potentials = new Dictionary<TOutput, double>(_functionMap.Count);

            // determine possibility for each given output
            foreach (var pair in _functionMap)
            {
                var pot = pair.Key(input);
                if (potentials.ContainsKey(pair.Value))
                    potentials[pair.Value] += pot;
                else
                    potentials[pair.Value] = pot;
            }

            // determine normalization value; if 0 or less, there's no possible output
            var potMax = potentials.Values.Sum();
            if (potMax <= 0.0)
            {
                output = default(TOutput);
                return false;
            }

            // normalize potentials to range 0...1
            foreach (var key in potentials.Keys)
                potentials[key] = potentials[key] / potMax;

            // get our random value and figure out which output to return
            var fuzz = _fuzzer.NextDouble();
            foreach (var key in potentials.Keys)
            {
                var pot = potentials[key];

                if (fuzz <= pot)
                {
                    output = key;
                    return true;
                }
                else
                {
                    fuzz -= pot;
                }
            }

            // if we're here, there's no potential results (aka we screwed up)
            // throw InvalidOperationException
            throw new InvalidOperationException("couldn't determine a correct value, please report to devs");
        }
    }
}
