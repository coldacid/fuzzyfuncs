﻿// FuzzyFuncs: A C# library for generic fuzzy logic systems
// Copyright (c) 2013 Chris Charabaruk <chrisNOSPAM@charabaruk.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Charabaruk.FuzzyFuncs
{
    public static class FunctionBuilder
    {
        internal static double Restrict(double input, double min, double max)
        {
            return Math.Min(Math.Max(input, min), max);
        }

        internal static Func<double, double> Invert(Func<double, double> func)
        {
            return i => Math.Abs(1.0 - func(i));
        }

        public static Func<double, double> AlwaysZero()
        {
            return i => 0.0;
        }

        public static Func<double, double> AlwaysOne()
        {
            return i => 1.0;
        }

        public static Func<double, double> LinearPeakUp(double peakStart, double peakTipStart, double peakTipEnd, double peakEnd)
        {
            if (peakStart > peakTipStart || peakTipStart > peakTipEnd || peakTipEnd > peakEnd)
                throw new ArgumentException("No arguments cannot be smaller than the preceding arguments");

            return i =>
            {
                if (i < peakStart || i > peakEnd)
                    return 0.0;
                else if (i >= peakTipStart && i <= peakTipEnd)
                    return 1.0;
                else
                {
                    if (i >= peakStart && i < peakTipStart)
                    {
                        i -= peakStart;
                        peakTipStart -= peakStart;

                        return i / peakTipStart;
                    }
                    else // i > PeakTipEnd && i <= peakEnd
                    {
                        i -= peakTipEnd;
                        peakEnd -= peakTipEnd;

                        return 1.0 - (i / peakEnd);
                    }
                }
            };
        }

        public static Func<double, double> LinearPeakDown(double peakStart, double peakTipStart, double peakTipEnd, double peakEnd)
        {
            return Invert(LinearPeakUp(peakStart, peakTipStart, peakTipEnd, peakEnd));
        }

        public static Func<double, double> LinearIncrease(double outputAtZero, double outputAtOne)
        {
            if (outputAtOne <= outputAtZero)
                throw new ArgumentException("outputAtOne must be greater than outputAtZero");

            return LinearPeakUp(outputAtZero, outputAtOne, double.MaxValue, double.MaxValue);
        }

        public static Func<double, double> LinearDecrease(double outputAtOne, double outputAtZero)
        {
            if (outputAtOne >= outputAtZero)
                throw new ArgumentException("outputAtZero must be greater than outputAtOne");

            return LinearPeakDown(double.MinValue, double.MinValue, outputAtOne, outputAtZero);
        }
    }
}
